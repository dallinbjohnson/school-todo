// eslint-disable-next-line no-unused-vars
export default function ({store, ssrContext}) {
  const routes = [
    {
      path: '/',
      component: () => import('layouts/SimpleLayout.vue'),
      children: [
        {path: '', name: 'home', component: () => import('pages/Index.vue')},
      ]
    },
    {
      path: '/',
      component: () => import('layouts/MainLayout.vue'),
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: () => import('pages/dashboard'),
          children: [

          ]
        },
        {path: 'reports', component: () => import('pages/reports')},
        {path: 'boards', name: 'boards', component: () => import('pages/boards.vue')},
        {path: 'boards/cards/:board_id?', name: 'cards', component: () => import('pages/cards.vue')},

        {
          path: 'logout',
          name: 'logout',
          beforeEnter(to, from, next) {
            store.dispatch('auth/logout')
              .then(result => {
                console.log('logout:', result);
                next('/');
              })
              .catch(error => {
                console.log('error logout:', error);
                next();
              });
          },
        },
      ]
    },
    {
      path: '/demo',
      component: () => import('layouts/MainLayout.vue'),
      children: [
        {path: 'tabNavbar', name: 'tabNavbar', component: () => import('pages/testTabNavbar')},
        {path: 'vueDnD', name: 'vueDnD', component: () => import('components/vueDnD/vueDnD')},
      ]
    },
  ];

  // Always leave this as last one
  if (process.env.MODE !== 'ssr') {
    routes.push({
      path: '*',
      component: () => import('pages/Error404.vue')
    });
  }

  return routes;
}


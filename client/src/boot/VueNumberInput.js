import VueNumberInput from '@chenfengyuan/vue-number-input';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ({ /*app, router,*/ Vue }) => {
  Vue.component('vue-number-input', VueNumberInput);
};

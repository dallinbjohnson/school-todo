// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async context => {
    if(context.data['$push']){
      // replace _id string with current users _id
      context.data['$push'].comments.createdBy = '5fa1876954cca84c5199fb5b';
      context.data['$push'].comments.updatedBy = '5fa1876954cca84c5199fb5b';
    }
    return context;
  };
};

const { iff, fastJoin, checkContext } = require('feathers-hooks-common');
const createdByComment = require('./hooks/created-by-comment');
const sanitize = require('../../hooks/ir-chat-sanitize');

const lget = require('lodash.get');
const lset = require('lodash.set');


// const { populate } = require('feathers-graph-populate');
//
//
// const populates = {
//   card: {
//     service: 'cards',
//     nameAs: 'fullCard',
//     keyHere: 'card',
//     keyThere: '_id',
//     asArray: false,
//     params: {}
//   }
// };
// const namedQueries = {
//   withCard: {
//     card: {}
//   }
// };

const commentsUserResolvers = {
  joins: {
    // eslint-disable-next-line no-unused-vars
    setCommentsUser: $select => async (todo, context) => {
      let comments = lget(todo, 'comments', []);
      let user_ids = comments.map(comment => comment.createdBy);
      if (user_ids.length > 0) {
        let users_res = await context.app.service('users').find({
          query: {
            _id: user_ids
          }
        });
        let users = users_res.data;

        let fatjoin_comments = comments.map(comment => {
          return lset(Object.assign({}, comment),
            'createdBy',
            users.find(user => String(user._id) === String(comment.createdBy)) || comment.createdBy);
        });
        lset(todo, '_fastjoin.comments', fatjoin_comments);
      }
    },
    // eslint-disable-next-line no-unused-vars
    setOwners: $select => async (todo, context) => {
      let user_ids = lget(todo, 'owners', []);
      if (user_ids.length > 0) {
        let users_res = await context.app.service('users').find({
          query: {
            _id: user_ids
          }
        });
        let users = users_res.data;

        lset(todo, '_fastjoin.owners', users);
      }
    }
  }
};

const channelUpdateCheck = async (context) => {
  checkContext(context, 'after');

  if (context.result.card) {
    const card = await context.app.service('cards').get(context.result.card, {$fastJoinTodos: true});
    context.app.service('cards').emit('updated', card);
  }
};


// const userResolvers = {
//   joins: {
//     // eslint-disable-next-line no-unused-vars
//     setUser: $select => async (todo, context) => {
//       let user_create_id = lget(todo, 'createdBy');
//       if (user_create_id) {
//         let user = await context.app.service('users')._get(user_create_id);
//         lset(todo, '_fastjoin.createdBy', user);
//       }
//       let user_update_id = lget(todo, 'updatedBy');
//       if (user_update_id) {
//         let user = await context.app.service('users')._get(user_update_id);
//         lset(todo, '_fastjoin.updatedBy', user);
//       }
//     },
//     // setUpdatedBy: $select => async (todo, context) => {
//     //   let user_update_id = lget(todo, 'updatedBy');
//     //   if (user_update_id) {
//     //     let user = await context.app.service('users')._get(user_update_id);
//     //     lset(todo, '_fastjoin.updatedBy', user);
//     //   }
//     // }
//   }
// };


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      createdByComment(),
      sanitize({paths: ['data.$push.comments[0].comment']}),
    ],
    remove: []
  },

  after: {
    all: [
      // populate({ populates, namedQueries }),
      // iff(
      //   context => {
      //     return lget(context, 'params.$fastJoinTodoUser', false);
      //   },
      //   [
      //     fastJoin(userResolvers),
      //   ]
      // ),
      iff(
        context => {
          return lget(context, 'params.$fastJoinTodoCommentsUser', false);
        },
        fastJoin(commentsUserResolvers),
      ),
    ],
    find: [],
    get: [],
    create: [
      channelUpdateCheck,
    ],
    update: [
      channelUpdateCheck,
    ],
    patch: [
      channelUpdateCheck,
    ],
    remove: [
      channelUpdateCheck,
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

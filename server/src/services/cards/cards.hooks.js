const {iff, fastJoin} = require('feathers-hooks-common');

const lget = require('lodash.get');
const lset = require('lodash.set');

// const { populate } = require('feathers-graph-populate');
//
//
// const populates = {
//   todos: {
//     service: 'todos',
//     nameAs: 'cardTodos',
//     keyHere: '_id',
//     keyThere: 'card',
//     asArray: true,
//     params: {}
//   }
// };
// const namedQueries = {
//   withTodos: {
//     todos: {}
//   }
// };


const todosResolvers = {
  joins: {
    // eslint-disable-next-line no-unused-vars
    setTodos: $select => async (card, context) => {
      let card_id = lget(card, '_id');
      if (card_id) {
        let todos = await context.app.service('todos').find({
          query: {
            card: card_id
          },
          $fastJoinTodoCommentsUser: true,
        });

        lset(card, '_fastjoin.todos', todos.data);
      }
    }
  }
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      // populate({ populates, namedQueries }),
      iff(
        context => {
          return lget(context, 'params.$fastJoinTodos', false);
        },
        fastJoin(todosResolvers),
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

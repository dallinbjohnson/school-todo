const {iff, fastJoin} = require('feathers-hooks-common');

const lget = require('lodash.get');
const lset = require('lodash.set');
const lfind = require('lodash.find');


const sharedResolvers = {
  joins: {
    // eslint-disable-next-line no-unused-vars
    setShared: $select => async (board, context) => {
      let shared = lget(board, 'shared', []);
      let user_ids = shared.map(item => item.user);
      if (user_ids.length > 0) {
        let users_res = await context.app.service('users').find({
          query: {
            _id: user_ids
          }
        });
        let users = users_res.data;

        lset(board, '_fastjoin.shared', shared.map(item => {
          let user = lfind(users, {_id: item.user});
          return {
            ...item,
            user: user,
          };
        }));
      }
    }
  }
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      iff(
        context => {
          return lget(context, 'params.$fastJoinShared', false);
        },
        fastJoin(sharedResolvers),
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

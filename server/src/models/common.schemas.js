// Generic mongoose model definitions to be used in other models
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const uuid = () => {
  let dt = new Date().getTime();
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
};

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HSLA = new Schema({
  h: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  s: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  l: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  a: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
}, { _id : false });

const HSVA = new Schema({
  h: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  s: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  l: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  a: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
}, { _id : false });

const RGBA = new Schema({
  h: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  s: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  l: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  a: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
}, { _id : false });

const Color = new Schema({
  hue: {type: mongoose.Types.Decimal128, required: false, min: 0, max: 1},
  alpha: {type: mongoose.Types.Decimal128, required: false},
  hex: {type: String, required: false, match: /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/},
  hexa: {type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/},
  hsla: {HSLA},
  hsva: {HSVA},
  rgba: {RGBA}
}, { _id : false });

const Phone = new Schema({
  phoneType: {type: String, required: false},
  number: {
    input: {type: String, required: false},
    international: {type: String, required: false},
    national: {type: String, required: false},
    e164: {type: String, required: false},
    rfc3966: {type: String, required: false},
    significant: {type: String, required: false},
  },
  regionCode: {type: String, required: false},
  valid: {type: Boolean, required: false},
  possible: {type: Boolean, required: false},
  possibility: {type: String, required: false},
  isValid: {type: Boolean, required: false},
  country: {
    name: {type: String, required: false},
    iso2: {type: String, required: false},
    dialCode: {type: String, required: false},
    priority: {type: Number, required: false},
    areaCodes: {type: String, required: false},
  },
  canBeInternationallyDialled: {type: Boolean, required: false},
  type: {type: String, required: false},
});

const Images = new Schema({
  large: {
    ETag: {type: String, required: false},
    Location: {type: String, required: false},
    key: {type: String, required: false},
    Key: {type: String, required: false},
    Bucket: {type: String, required: false},
    details: {
      name: {type: String, required: false},
      size: {type: Number, required: false},
      type: {type: String, required: false},
      lastModifiedDate: {type: Date, required: false},
    }
  },
  medium: {
    ETag: {type: String, required: false},
    Location: {type: String, required: false},
    key: {type: String, required: false},
    Key: {type: String, required: false},
    Bucket: {type: String, required: false},
    details: {
      name: {type: String, required: false},
      size: {type: Number, required: false},
      type: {type: String, required: false},
      lastModifiedDate: {type: Date, required: false},
    }
  },
  small: {
    ETag: {type: String, required: false},
    Location: {type: String, required: false},
    key: {type: String, required: false},
    Key: {type: String, required: false},
    Bucket: {type: String, required: false},
    details: {
      name: {type: String, required: false},
      size: {type: Number, required: false},
      type: {type: String, required: false},
      lastModifiedDate: {type: Date, required: false},
    }
  },
  favicon: {
    ETag: {type: String, required: false},
    Location: {type: String, required: false},
    key: {type: String, required: false},
    Key: {type: String, required: false},
    Bucket: {type: String, required: false},
    details: {
      name: {type: String, required: false},
      size: {type: Number, required: false},
      type: {type: String, required: false},
      lastModifiedDate: {type: Date, required: false},
    }
  }
});

const Address = new Schema({
  address1: {type: String, required: true},
  city: {type: String, required: true},
  country: {type: String, required: true},
  formatted: {type: String, required: false},
  latitude: {type: Number, required: true},
  longitude: {type: Number, required: true},
  name: {type: String, required: false},
  postal: {type: String, required: true},
  region: {type: String, required: false},
  googleAddress: {type: Object, required: false},
  tags: {type: Object, required: false},
  type: {type: Object, required: false},
});

const Theme = new Schema({
  '--q-color-primary': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-secondary': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-accent': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-dark': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },

  '--q-color-positive': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-negative': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-info': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
  '--q-color-warning': { type: String, required: false, match: /^#([A-Fa-f0-9]{8}|[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/ },
}, { _id : false });

const Settings = new Schema({
  groups: {

  },
  todos: {

  },
  users: {

  },
});


module.exports = {
  uuid,
  Color,
  Phone,
  Images,
  Address,
  Theme,
  Settings
};


// todos-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const modelName = 'todos';
  const mongooseClient = app.get('mongooseClient');
  const {Schema} = mongooseClient;
  const schema = new Schema({
    name: {type: String, required: true},
    description: {type: String},
    order: {type: Number, required: true},
    owners: [{type: Schema.Types.ObjectId, ref: 'users'}],
    priority: {type: String, enum: ['critical', 'high', 'medium', 'low', null], default: null, lowercase: true, trim: true},
    category: {type: String, enum: ['bug', 'feature', 'task', 'question', 'research', 'other'], lowercase: true, trim: true},
    tags: [{type: String}],
    parent: {type: Schema.Types.ObjectId, ref: 'todos'},
    children: [{type: Schema.Types.ObjectId, ref: 'todos'}],
    comments: [new Schema({
      comment: {type: String, required: true},
      createdBy: {type: Schema.Types.ObjectId, ref: 'users', required: true},
      updatedBy: {type: Schema.Types.ObjectId, ref: 'users', required: true},
    }, {
      timestamps: true,
    })],

    card: {type: Schema.Types.ObjectId, ref: 'cards', required: true},
    createdBy: {type: Schema.Types.ObjectId, ref: 'users'},
    updatedBy: {type: Schema.Types.ObjectId, ref: 'users'},

    deleted: {type: Boolean, default: false},
    onServerAt: { type: Date },
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);

};

const {fastJoin} = require('feathers-hooks-common');
const lget = require('lodash.get');
const lset = require('lodash.set');

const nestedfJoinHook = function (path, service, idKey) {
  let resolver = {
    joins: {
      // eslint-disable-next-line no-unused-vars
      set: $select => async (data, context) => {
        let items = lget(data, path, []);
        let ids = items.map(item => item[idKey]);
        if (ids.length > 0) {
          let items_res = await context.app.service(`${service}`).find({
            query: {
              _id: ids
            }
          });
          let queriedItems = items_res.data;
          let fastJoin_items = items.map(item => {
            return lset(Object.assign({}, item),
              idKey,
              queriedItems.find(query => String(query._id) === String(item[idKey]) || item[idKey])
            );
          });

          lset(data, `_fastjoin.${path}`, fastJoin_items);
        }

      }
    }
  };
  return fastJoin(resolver);
};

module.exports = {
  nestedfJoinHook
};

// Application hooks that run for every service
const {softDelete, paramsFromClient, iff} = require('feathers-hooks-common');
const { 'paramsFromClient': fGraphParams } = require('feathers-graph-populate');

const ServiceLogger = require('./hooks/service-logger');
const removeFastjoin = require('./hooks/remove-fastjoin');
const restQueryUnstringify = require('./hooks/rest-query-unstringify');
const createdBy = require('./hooks/created-by');
const updatedBy = require('./hooks/updated-by');


module.exports = {
  before: {
    all: [
      restQueryUnstringify(),
      paramsFromClient('disableSoftDelete', '$fastJoinTodos', '$fastJoinShared', '$fastJoinTodoUser'),
      ServiceLogger({LEVEL: 'warning'}),
      iff(
        context => ![
          'authentication',
          'geocode',
          'places',
          'places-auto-complete',
          'places-query-auto-complete',
          'file-uploader',
          'uploads',
          ...Object.values(context.app.get('uploads').enums.UPLOAD_SERVICES)
        ].includes(context.path),
        [
          softDelete(),
        ]
      ),
      fGraphParams('$populateParams'),
      removeFastjoin(),
    ],
    find: [],
    get: [],
    create: [createdBy(), updatedBy()],
    update: [updatedBy()],
    patch: [updatedBy()],
    remove: [updatedBy()]
  },

  after: {
    all: [
      ServiceLogger({LEVEL: 'info'}),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [
      ServiceLogger({LEVEL: 'info'}),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
